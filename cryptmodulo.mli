(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Some auxiliary functions for modular arithmetic *)

open Big_int

val is_odd_big_int : big_int -> bool
    (* fast test whether the number is odd *)

val shift_1_right_big_int : big_int -> big_int
    (* divides the non-negative number by 2 *)


val square_mod : big_int -> big_int -> big_int
    (* square_mod n x == x ^ 2 mod n *)

val power_mod : big_int -> big_int -> big_int -> big_int
    (* power_mod n x y == x ^ y mod n *)

val inverse_mod : big_int -> big_int -> big_int
    (* inverse_mod n x == x ^ -1 mod n. Raises Not_found if there is no
     * solution.
     *)


