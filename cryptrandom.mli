(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Generate big random numbers with cryptographic quality *)

open Big_int

val init_random : string -> unit
  (* initializes the generator with the given string. The string can be
   * arbitrary material, it should come from a random source, and the
   * space for this string should have at least as many elements as the
   * resulting number space.
   * Example:
   * To generate 1024 bit numbers, there should be 2^1024 possible strings
   * as input to this function. If you take the microseconds between
   * keystrokes as inputs (from 0 to 255), you need at least 128 keystrokes.
   *)

val next_random : int -> big_int
  (* Get the next random number with the given number of bits. 
   * The most significant bit is always 1, i.e.
   * 2 ^ (n-1)  <=  next_random n  <  2 ^ n
   * This is a slow function.
   *)

val next_prime : int -> big_int
  (* Get a prime number with the given number of bits.
   * The most significant bit is always 1, i.e.
   * 2 ^ (n-1)  <=  next_random n  <  2 ^ n
   * This is a very slow function. 
   * NOTE: There is a very small chance that the number is not prime.
   *)



