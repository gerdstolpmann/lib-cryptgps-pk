(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Generate big random numbers with cryptographic quality *)

open Big_int
open Cryptmodulo

let buf = ref "";;


let init_random s =
  buf := Digest.string s
;;


let next_random bits =
  let rec next x bits =
    if bits = 0 then
      x
    else begin
      buf := Digest.string !buf;
      if Char.code(!buf.[0]) mod 2 = 1 then
	next (succ_big_int (mult_int_big_int 2 x)) (bits-1)
      else
	next (mult_int_big_int 2 x) (bits-1)
    end
  in
  next unit_big_int (bits-1)
;;


let primes_to n =
  let a = Array.create n true in
  a.(0) <- false;
  a.(1) <- false;
  for k = 2 to n/2 do
    let j = ref 2 in
    while k * !j < n do
      a.(k * !j) <- false;
      incr j
    done;
  done;

  let rec collect k =
    if k >= n then
      []
    else
      if a.(k) then
	k :: collect (k+1)
      else
	collect(k+1)
  in
  collect 2
;;


let rec next_prime bits =
  (* Rabin-Miller test *)
  let p0 = next_random bits in
  let p =
    if is_odd_big_int p0 then p0 else succ_big_int p0 in

  prerr_endline ("Testing " ^ string_of_big_int p);

  if List.exists
       (fun q -> sign_big_int(mod_big_int p (big_int_of_int q)) = 0)
       (primes_to 256)
  then
    next_prime bits
  else begin

    prerr_endline "Simple tests passed";

    let rec get_b_and_m b p' =
      if is_odd_big_int p' then 
	b, p'
      else
	get_b_and_m (b+1) (shift_1_right_big_int p')
    in

    let p_minus_1 = sub_big_int p unit_big_int in
    let b, m = get_b_and_m 0 p_minus_1 in
    
    let rabin_miller a =
      let j = ref 1 in
      let z = ref (power_mod p a m) in
      if eq_big_int !z unit_big_int   or   eq_big_int !z p_minus_1 then
	()

      else begin
	while  !j < b  &  not (eq_big_int !z p_minus_1) do
	  z := square_mod p !z;
	  if eq_big_int !z unit_big_int then raise Not_found;
	  incr j
	done;

	if eq_big_int !z p_minus_1 then
	  ()
	else
	  raise Not_found
      end
    in

    let fewer_bits = bits / 4 in
    try
      rabin_miller (next_random fewer_bits);
      prerr_endline "Test A passed";
      rabin_miller (next_random fewer_bits);
      prerr_endline "Test B passed";
      rabin_miller (next_random fewer_bits);
      prerr_endline "Test C passed";
      rabin_miller (next_random fewer_bits);
      prerr_endline "Test D passed";
      rabin_miller (next_random fewer_bits);
      prerr_endline "Test E passed";
      p
    with
      Not_found -> next_prime bits
  end
;;


