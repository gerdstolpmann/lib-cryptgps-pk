# make all: 		make bytecode archive
# make opt: 		make native archive
# make install: 	install bytecode archive, and if present, native archive
# make uninstall: 	uninstall package
# make clean: 		remove intermediate files
# make distclean: 	remove any superflous files
# make release: 	cleanup, create archive, tag CVS module 
#			(for developers)

#----------------------------------------------------------------------
# specific rules for this package:

OBJECTS  = cryptmodulo.cmo cryptrandom.cmo crypt_rsa.cmo
XOBJECTS = cryptmodulo.cmx cryptrandom.cmx crypt_rsa.cmx
ARCHIVE  = cryptgps-pk.cma
XARCHIVE = cryptgps-pk.cmxa
NAME     = cryptgps-pk
REQUIRES = 

all: $(ARCHIVE)

opt: $(XARCHIVE)

$(ARCHIVE): $(OBJECTS)
	$(OCAMLC) -a -o $(ARCHIVE) $(OBJECTS)

$(XARCHIVE): $(XOBJECTS)
	$(OCAMLOPT) -a -o $(XARCHIVE) $(XOBJECTS)

#----------------------------------------------------------------------
# general rules:

OPTIONS   =
OCAMLC    = ocamlc $(OPTIONS) $(ROPTIONS)
OCAMLOPT  = ocamlopt $(OPTIONS) $(ROPTIONS)
OCAMLDEP  = ocamldep $(OPTIONS)
OCAMLFIND = ocamlfind

depend: *.ml *.mli
	$(OCAMLDEP) *.ml *.mli >depend

depend.pkg: Makefile
	$(OCAMLFIND) use -p ROPTIONS= $(REQUIRES) >depend.pkg

.PHONY: install
install: all
	{ test ! -f $(XARCHIVE) || extra="*.cmxa *.a"; }; \
	$(OCAMLFIND) install $(NAME) *.mli *.cmi *.cma RELEASE $$extra

.PHONY: uninstall
uninstall:
	$(OCAMLFIND) remove $(NAME)

.PHONY: clean
clean:
	rm -f *.cmi *.cmo *.cma *.cmx *.o *.a *.cmxa

.PHONY: distclean
distclean: clean
	rm -f *~ depend depend.pkg

.PHONY: dist
dist:
	r=`head -1 RELEASE`; cd ..; gtar czf $(NAME)-$$r.tar.gz  --exclude='*/CVS*' --exclude="*/depend.pkg" --exclude="*/depend" $(NAME)

.PHONY: tag-release
tag-release:
	r=`head -1 RELEASE | sed -e s/\\\./-/g`; cd ..; cvs tag -F $(NAME)-$$r $(NAME)

.PHONY: release
release: distclean
	$(MAKE) tag-release
	$(MAKE) dist

.ml.cmx:
	$(OCAMLOPT) -inline 10000 -c $<

.ml.cmo:
	$(OCAMLC) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

.SUFFIXES: .cmo .cmi .cmx .ml .mli

include depend
include depend.pkg