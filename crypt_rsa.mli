(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* CORE RSA ALGORITHM *)

val get_key_pair : int -> int -> (Big_int.big_int * Big_int.big_int)

(* let (d,n) = get_key_pair e b
 *
 * e should be 3, 17, or 65537.
 *
 * This function determines a (random) pair of a private and a public key.
 * The number d is the private key; the numbers n and e are public.
 *
 * The number b determines the length of the keys:
 *   2 ^ (2b-2)  <=  n  <   2 ^ 2b
 * Furthermore,
 *   0  <=  d  <  n
 *
 * The number n contains 2b-2 bits that are randomly chosen.
 *
 * To encrypt a message m with 0 <= m < n, compute
 * c = Cryptmodulo.power_mod n m e
 *
 * To decrypt the message c:
 * m = Cryptmodule.power_mod n c d
 *
 * That means: To encrypt a message with r bits, choose b = r/2, but
 * large enough (e.g. b = 512 yielding 1024-bit private keys).
 * Shorter messages must be padded with random data. Longer messages
 * should be broken into several blocks.
 *
 * IMPORTANT NOTE: Although it is very unlikely, it may happen that
 * the calculated keys do not work (because the underlying prime
 * number generator is probabilistic).
 *)  

