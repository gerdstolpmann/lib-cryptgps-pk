(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


open Cryptmodulo
open Cryptrandom
open Big_int

let rec get_key_pair e bits =
  let p = next_prime bits in
  let q = next_prime bits in

  let m = mult_big_int (pred_big_int p) (pred_big_int q) in

  try
    let d = inverse_mod m (big_int_of_int e) in     (* may raise Not_found *)
    (* d is the private key *)

    let n = mult_big_int p q in
    
    d, n
  with
      Not_found -> get_key_pair e bits
	  (* p and q did not work *)
;;


