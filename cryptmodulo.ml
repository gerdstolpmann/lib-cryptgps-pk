(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

(* Some auxiliary functions for modular arithmetic *)

open Big_int
open Nat

let square_mod n x =
  (* computes x * x mod n *)
  mod_big_int (square_big_int x) n
;;


let is_odd_big_int x = 
  is_digit_odd (nat_of_big_int x) 0
;;


let shift_1_right_big_int x =
  let h = create_nat 1 in
  let x_nat = nat_of_big_int x in
  shift_right_nat x_nat 0 (num_digits_big_int x) h 0 1;
  big_int_of_nat x_nat
;;


let shift_left_big_int x n =  
  (* TODO: can be optimized to shift up to 32 bits per recursion *)
  let h = create_nat 1 in
  set_to_zero_nat h 0 1;
  let rec shift x_nat n =
    if n = 0 then
      big_int_of_nat x_nat
    else begin
      shift_left_nat x_nat 0 (length_nat x_nat) h 0 1;
      if not (is_digit_zero h 0) then begin
	let l = length_nat x_nat in
	let x'_nat = create_nat (l + 1) in
	blit_nat x'_nat 0 x_nat 0 l;
	blit_nat x'_nat l h 0 1;
	set_to_zero_nat h 0 1;
	shift x'_nat (n-1)
      end
      else
	shift x_nat (n-1)
    end
  in
  shift (nat_of_big_int x) n
;;


let power_mod n x y =
  (* computes x ^ y mod n  for positive x, non-negative y *)
  let s = ref unit_big_int in
  let t = ref x in
  if is_int_big_int y then begin
    let u = ref (int_of_big_int y) in
    while !u > 0 do
      if (!u land 1) > 0 then
	s := mod_big_int (mult_big_int !s !t) n;
      u := !u lsr 1;
      t := square_mod n !t;
    done
  end
  else begin
    let u = ref y in
    while gt_big_int !u zero_big_int do
      if is_odd_big_int !u then
	s := mod_big_int (mult_big_int !s !t) n;
      u := shift_1_right_big_int !u;
      t := square_mod n !t;
    done;
  end;
  
  !s
;;
    

let extended_euclid u v =
  (* u, v > 0 *)

  let swap a b =
    let c = !a in
    a := !b;
    b := c
  in

  let rec ext_euclid2 u v k = 
    (* u > v,  u or v not divisible by 2 *)
    let u1 = ref unit_big_int in
    let u2 = ref zero_big_int in
    let u3 = ref u in
    let t1 = ref v in
    let t2 = ref (pred_big_int u) in
    let t3 = ref v in
    
    while (sign_big_int !t3 > 0) do

      let cont = ref true in
      while !cont do
	if not (is_odd_big_int !u3) then begin
	  if is_odd_big_int !u1 or is_odd_big_int !u2 then begin
	    u1 := add_big_int !u1 v;
	    u2 := add_big_int !u2 u;
	  end;
	  u1 := shift_1_right_big_int !u1;
	  u2 := shift_1_right_big_int !u2;
	  u3 := shift_1_right_big_int !u3;
	end;
	if (not (is_odd_big_int !t3)) or lt_big_int !u3 !t3 then begin
	  swap u1 t1;
	  swap u2 t2;
	  swap u3 t3;
	end;
	cont := not (is_odd_big_int !u3)
      done;
      
      while lt_big_int !u1 !t1 or lt_big_int !u2 !t2 do
	u1 := add_big_int !u1 v;
	u2 := add_big_int !u2 u;
      done;

      u1 := sub_big_int !u1 !t1;
      u2 := sub_big_int !u2 !t2;
      u3 := sub_big_int !u3 !t3;

    done;

    while ge_big_int !u1 v & ge_big_int !u2 u do
      u1 := sub_big_int !u1 v;
      u2 := sub_big_int !u2 u;
    done;

    shift_left_big_int !u1 k,
    shift_left_big_int !u2 k,
    shift_left_big_int !u3 k
	
  in

  let rec ext_euclid1 u v k =  
    (* u > v *)
    if is_odd_big_int u or is_odd_big_int v then
      ext_euclid2 u v k
    else
      ext_euclid1 (shift_1_right_big_int u) (shift_1_right_big_int v) (k+1)
  in

  if lt_big_int u v then
    ext_euclid1 v u 0
  else
    ext_euclid1 u v 0
;;


let inverse_mod n x =
  (* computes x ^ -1 mod n, or raises Not_found *)
  let a, b, gcd = extended_euclid n x in
  if eq_big_int gcd unit_big_int then
    sub_big_int n b
  else
    raise Not_found
;;



